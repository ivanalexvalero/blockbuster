//
//  TheMovieDBAPI.swift
//  TheMovieDBAPI
//
//  Created by Bryan A Bolivar M on 25/10/22.
//

import Foundation
import Moya

public enum TheMovieDBAPI {
    case movieslist
    case actorpopular
    case actorDetail(Int)
    case searchMovie(search: String?)
    case moviesReviews(Int)
    case movieTrends
    case tvshows
}

extension TheMovieDBAPI {
    var apiKey: String {
        return "627e3f67dac2c73e662093fe66b67ce7"
    }
}

extension TheMovieDBAPI: TargetType {

    public var baseURL: URL { return URL(string: "https://api.themoviedb.org/3")! }

    public var path: String {
        switch self {
        case .movieslist:
            return "/movie/popular"
        case .actorpopular:
            return "/person/popular"
        case .actorDetail(let person_id):
            return "/person/\(person_id)"
        case .moviesReviews(let id):
            return "/movie/\(id)/reviews"
        case .movieTrends:
                    return "/trending/all/day"
        case .searchMovie(search: _):
            return "/search/movie"
        case .tvshows:
            return "/tv/popular"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .movieslist, .searchMovie(search: _), .actorpopular, .moviesReviews, .tvshows,  .movieTrends, .actorDetail:
            return .get
        }
    }
    
    public var task: Task {
        switch self {
        case .movieslist, .actorpopular, .moviesReviews, .tvshows, .movieTrends, .actorDetail:
            return .requestParameters(parameters: ["api_key": apiKey], encoding: URLEncoding.default)
        case .searchMovie(search: let search):
            return .requestParameters(parameters: ["api_key": apiKey, "query": search!], encoding: URLEncoding.default)
        }
    }

    public var headers: [String: String]? {
        return [:]
    }

    public var sampleData: NSData {
        switch self {
        case .movieslist, .searchMovie(search: _), .actorpopular, .moviesReviews, .tvshows, .movieTrends, .actorDetail:
            return "{}".data(using: String.Encoding.utf8)! as NSData
        }
    }
}
