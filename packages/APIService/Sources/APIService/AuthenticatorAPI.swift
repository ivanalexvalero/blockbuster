//
//  AuthenticatorAPI.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 26/10/22.
//

import Foundation
import Moya

public enum AuthenticatorAPI {
    /// Used for performing sign in request
    ///
    case login(email: String, password: String)
    case trailers
    
}

extension AuthenticatorAPI: TargetType {

    public var baseURL: URL { return URL(string: "http://localhost:8080")! }

    public var path: String {
        switch self {
        case .login:
            return "/login"
        case .trailers:
            return "/trailers"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .trailers:
            return .get
        }
    }
    
    public var task: Task {
        switch self {
        case .login(let email, let password):
            return .requestParameters(parameters: ["email": email, "password": password], encoding: URLEncoding.default)
        case .trailers:
            return .requestParameters(parameters: [:], encoding: URLEncoding.default)

        }
    }

    public var headers: [String: String]? {
        return ["Content-Type" : "application/json; charset=utf-8"]
    }

    public var sampleData: Data {
        switch self {
        case .login, .trailers:
            return "{}".data(using: String.Encoding.utf8)! as Data
        }
    }
}

