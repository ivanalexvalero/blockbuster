// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SystemDesign",
    defaultLocalization: "en",
    platforms: [
        .iOS(.v15)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SystemDesign",
            targets: ["SystemDesign"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://github.com/onevcat/Kingfisher.git", from: "7.0.0"),
        .package(url: "https://github.com/SnapKit/SnapKit", branch: "develop"),
        .package(path: "../Model")
        
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SystemDesign",
            dependencies: ["Kingfisher", "Model"],
            resources: [.process("Resources")]),
        .testTarget(
            name: "SystemDesignTests",
            dependencies: ["SystemDesign"]),
    ]
)
