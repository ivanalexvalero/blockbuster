//
//  File.swift
//  
//
//  Created by Yanet Rodriguez on 14/11/2022.
//

import Foundation

public enum Language: String {
    case enterEmail
    case enterPassword
    case confirmPassword
    case signIn
    case signUp
    case yourAge
    case recoverPassword
    case welcome
    case greeting
    case farewell
    case addFavourite
    case removeFavourite
}

extension Language {
    public var localizedValue: String {
        switch self {
        case .enterEmail:
            return "Enter your email"
        case .enterPassword:
            return "Enter your password"
        case .confirmPassword:
            return "Please confirm your password"
        case .signIn:
            return "Sign In"
        case .signUp:
            return "Sign Up"
        case .yourAge:
            return "Please enter your age"
        case .recoverPassword:
            return "Send email"
        case .welcome:
            return "Welcome to Blockbuster"
        case .greeting:
            return "It's nice to see you again"
        case .farewell:
            return "See you soon"
        case .addFavourite:
            return "Add to favourites"
        case .removeFavourite:
            return "Remove from favourites"
        }
    }
    
    public var localized: String {
        return NSLocalizedString(self.rawValue, comment: self.rawValue)
    }
}
