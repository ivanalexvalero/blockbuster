//
//  SwiftUIView.swift
//  
//
//  Created by Bryan A Bolivar M on 28/10/22.
//

import SwiftUI
import Kingfisher

public struct TrailerView: View {
    @State var name: String = ""
    var image: String
    @State var url: String = ""
    var action: (String) -> ()
    
    public var body: some View {
        Button {
            action(url)
            var newName = name
            name.append("1")
        } label: {
            VStack {
                RoundedRectangle(cornerRadius: 24)
                    .frame(width: 100, height: 100)
                    .foregroundColor(.gray)
                    .overlay (
                        KFImage(URL(string: image)!)
                            .resizable()
                            .foregroundColor(.black)
                            .cornerRadius(24)
                        
                    )
                Text(name)
                    .foregroundColor(.black)
                    .font(.title)
            }
        }

    }
}

extension TrailerView {
    public static func viewRepresentation(url: String, name: String, image: String, viewController: UIViewController, action: @escaping (String) -> ()) -> UIHostingController<TrailerView> {
        let childView = UIHostingController(rootView: TrailerView(name: name, image: image, url: url, action: action))
        viewController.view.addSubview(childView.view)
        childView.didMove(toParent: viewController)
        return childView
    }
}

struct TrailerView_Previews: PreviewProvider {
    static var previews: some View {
        TrailerView(name: "Dr. House", image: "person.circle", url: "") { url in
            print(url)
        }
    }
}
