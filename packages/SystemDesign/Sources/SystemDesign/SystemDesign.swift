import UIKit
import Kingfisher

public struct SystemDesign {
    public private(set) var text = "Hello, World!"

    public init() {
        
    }
}

extension UIButton {
   public enum Style {
        case orange
    }
    
    static var orangeButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.setTitleColor(.orange, for: .normal)
        return button
    }()
    
    public func adoptStyle(style: Style) {
        switch style {
            case .orange:
            self.setTitleColor(.orange, for: .normal)
        }
    }
}

extension UIImageView {
    public static func appLogoImageView() -> UIImageView  {
        let imageView = UIImageView(frame: .zero)
        let url = URL(string: "https://image.tmdb.org/t/p/w500/z2yahl2uefxDCl0nogcRBstwruJ.jpg")
        imageView.kf.setImage(with: url)
        return imageView
    }
}


public class LoadingIndicator: UIView {
    public static let spinner = UIActivityIndicatorView(style: .large)
}

public enum Image: String, CaseIterable {
    case camera = "icon_camera"
    case chair = "icon_chair"
    case cinema = "icon_cinema"
    case popcorn = "icon_popcorn"
    case tape = "icon_tape"
}

public extension Image {
    
    func image() -> UIImage {
        let bundle = Bundle.module
        guard let image = UIImage(named: rawValue, in: bundle, compatibleWith: nil) else {
            return UIImage()
        }
        return image
    }
}

//MARK: Custom Colors
public enum MovieColors: String, CaseIterable {
    case movieRed
    case movieBlue
    case movieBlack
    case movieGreen
    case movieGrey
    case movieLightBlue
    case movieWhite
    case movieYellow
    
    var name: String {
        self.rawValue
    }
}
extension UIColor {
    static let movieRed = UIColor(named: "movieRed",in: Bundle.module, compatibleWith: nil)
    static let movieBlue = UIColor(named: "movieBlue",in: Bundle.module, compatibleWith: nil)
    static let movieBlack = UIColor(named: "movieBlack",in: Bundle.module, compatibleWith: nil)
    static let movieGreen = UIColor(named: "movieGreen",in: Bundle.module, compatibleWith: nil)
    static let movieGrey = UIColor(named: "movieGrey",in: Bundle.module, compatibleWith: nil)
    static let movieLightBlue = UIColor(named: "movieLightBlue",in: Bundle.module, compatibleWith: nil)
    static let movieWhite = UIColor(named: "movieWhite",in: Bundle.module, compatibleWith: nil)
    static let movieYellow = UIColor(named: "movieYellow",in: Bundle.module, compatibleWith: nil)
}
