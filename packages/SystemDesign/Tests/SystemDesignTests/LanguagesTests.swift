//
//  File.swift
//  
//
//  Created by Yanet Rodriguez on 14/11/2022.
//

import Foundation

import Foundation
import XCTest
@testable import SystemDesign

final class LanguageTests: XCTestCase {

    func testCopyTextsAreCorrect() {
        XCTAssertEqual(Language.enterEmail.localizedValue, "Enter your email")
        XCTAssertEqual(Language.enterPassword.localizedValue, "Enter your password")
        XCTAssertEqual(Language.signIn.localizedValue, "Sign In")
        XCTAssertEqual(Language.confirmPassword.localizedValue, "Please confirm your password")
        XCTAssertEqual(Language.welcome.localizedValue, "Welcome to Blockbuster")
        XCTAssertEqual(Language.farewell.localizedValue, "See you soon")
        XCTAssertEqual(Language.addFavourite.localizedValue, "Add to favourites")
    }
    
}
