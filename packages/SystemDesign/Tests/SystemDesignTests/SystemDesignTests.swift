import XCTest
@testable import SystemDesign

final class SystemDesignTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SystemDesign().text, "Hello, World!")
    }
}
