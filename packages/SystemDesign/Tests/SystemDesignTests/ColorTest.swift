import XCTest
@testable import SystemDesign

final class ColorTest: XCTestCase {
    
    func testColorExistsInAssetCatalog() {
        for type in MovieColors.allCases {
            let color = UIColor(named: type.name, in: Bundle.module, compatibleWith: nil)
            XCTAssertNotNil(color, "Asset catalog is missing an entry for \(type.name)")
        }
    }

}
