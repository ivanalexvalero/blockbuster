//
//  File.swift
//  
//
//  Created by Cristian Gonzalez on 14/11/2022.
//

import Foundation
import XCTest
@testable import SystemDesign

final class AssetsTest: XCTestCase {
    
    func testWhenIconNameIsEqual() {
        let icon = Image.camera.image()
        let bundle = Bundle.module
        let image = UIImage(named: "icon_camera", in: bundle, compatibleWith: nil)
        XCTAssertTrue(icon.pngData() == image?.pngData())
        }
    
    func testWhenCameraCaseIsNotNil() {
        let icon = Image.camera.image()
        let bundle = Bundle.module
        let image = UIImage(named: "icon_camera", in: bundle, compatibleWith: nil)
        XCTAssertNotNil(icon)
        }
    
    func testWhenAllImageLoads(){
            Image.allCases.forEach { (image) in
            let img = image.image()
                if img == UIImage() {
                    XCTFail("Unable to load asset: \(image.rawValue)")
                }
            }
        }
    }
