//
//  Media.swift
//  Blockbuster
//
//  Created by u633168 on 26/10/2022.
//

import Foundation
import UIKit

public struct Media: Codable {
    
    public init(name: String?, title: String?, description: String, image: String? = nil) {
        self.name = name
        self.title = title
        self.description = description
        self.image = image
    }
    
    public var dictionaryRepresentation: [String: Any] {
        return [
            "name": name,
            "title": title,
            "description": description,
            "imageURL": image ?? ""
        ]
    }
    
    /// The movie Title
    public let name: String?
    public let title: String?
    /// The movie overview
    public let description: String

    
    /// The poster Path
    public let image: String?
    
    public enum CodingKeys: String, CodingKey {
        case name = "name"
        case title = "title"
        case description  = "overview"
        //case genre
        case image = "poster_path"
    }
    
    public func movieDetails(age: Int) -> String {
        return ""
    }
    
    public func imageURLRepresentation() -> URL? {
        guard let image = image else {
            return Constants.defaultImageURL
        }
        return URL(string:  "https://image.tmdb.org/t/p/w500/" + image)
    }
    
}

