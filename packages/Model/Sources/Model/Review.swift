//
//  Reviews.swift
//  Blockbuster
//
//  Created by Juleanny Navas on 26/10/2022.
//

import Foundation
import UIKit

// MARK: - ReviewsResponse
public struct ReviewResponse: Codable {
    public var id: Int
    public var page: Int
    public var results: [Review]
    public var totalPages: Int
    public var totalResults: Int

    public enum CodingKeys: String, CodingKey {
        case id
        case page
        case results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

// MARK: - Result
public struct Review: Codable, Hashable {
    public var author: String
    public var authorDetails: AuthorDetails
    public var content: String
    public var createdAt: String
    public var id: String
    public var updatedAt: String
    public var url: String

    public enum CodingKeys: String, CodingKey {
        case author
        case authorDetails = "author_details"
        case content
        case createdAt = "created_at"
        case id
        case updatedAt = "updated_at"
        case url
    }
    
    public func hash(into hasher: inout Hasher) {
        
    }
    
    public static func == (lhs: Review, rhs: Review) -> Bool {
        return lhs.author == rhs.author && lhs.content == rhs.content && lhs.createdAt == rhs.createdAt && lhs.id == rhs.id && lhs.updatedAt == rhs.updatedAt && lhs.url == rhs.url
    }
}

// MARK: - AuthorDetails
public struct AuthorDetails: Codable {
    public var name: String
    public var username: String
    public var avatarPath: String?
    public var rating: Int

    public enum CodingKeys: String, CodingKey {
        case name
        case username
        case avatarPath = "avatar_path"
        case rating
    }
}

extension AuthorDetails: ImageRepresentable {
    public var path: String? {
        return avatarPath
    }
}

