//
//  TvShow.swift
//  Blockbuster
//
//  Created by Mateo Luchina on 26/10/2022.
//


import Foundation
import UIKit

public struct TvShow: Codable {
    
    public let name: String
    public let description: String
    public let image: String?
    public let rating: Double
    
    public init(name: String, description: String, image: String? = nil, rating: Double) {
        self.name = name
        self.description = description
        self.image = image
        self.rating = rating
    }
    
    public enum CodingKeys: String, CodingKey {
        case name = "name"
        case description  = "overview"
        case image = "poster_path"
        case rating = "vote_average"
    }
    
    public var dictionaryRepresentation: [String: String] {
        return [
            "name": name,
            "about": description,
            "rating": String(rating),
            "imageURL": image ?? ""
        ]
    }
    
    public func movieDetails(age: Int) -> String {
        return ""
    }
    
    public func imageURLRepresentation() -> URL? {
        guard let image = image else {
            return Constants.defaultImageURL
        }
        return URL(string:  "https://image.tmdb.org/t/p/w500/" + image)
    }
    
}

