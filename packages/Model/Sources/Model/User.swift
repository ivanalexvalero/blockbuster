//
//  User.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 19/10/22.
//

import Foundation

public class User {
    private let userName: String
    private let userEmail: String
    public var age: Int = 0
    
    public init(name: String, email: String) {
        self.userName = name
        self.userEmail = email
    }
}


let defaultUser1 = User(name: "bryan", email: "bolivar@gmail.com")
