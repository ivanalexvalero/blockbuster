//
//  Trailer.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 28/10/22.
//

import Foundation

public struct Trailer: Codable {
    public let name: String
    public let image: String
    public let url:  String
    
    public var urlRoute: URL? {
        return URL(string: url)
    }
}

extension Trailer: ImageRepresentable {
    public var path: String? {
        return image
    }
}
