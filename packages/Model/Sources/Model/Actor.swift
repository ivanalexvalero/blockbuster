//
//  Actor.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 26/10/2022.
//

import Foundation
import UIKit

public struct Actor: Codable {
    
    public init(name: String, popularity: Double, id: Int, image: String? = nil) {
        self.name = name
        self.popularity = popularity
        self.id = id
        self.image = image
    }
    
    public var dictionaryRepresentation: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["name"] = name
        dictionary["popularity"] = popularity
        dictionary["id"] = id
        
        if let image = image {
            dictionary["imageURL"] = String(image)
        } else {
            dictionary["imageURL"] = Constants.defaultImageURL.absoluteString
        }
        
        return dictionary
    }
    
    /// The movie Title
    public let name: String
    
    /// The movie overview
    public let popularity: Double
    
    //var genre: MovieGenre = .action
    
    /// The poster Path
    public let image: String?
    
    public let id: Int
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case popularity  = "popularity"
        case image = "profile_path"
        case id = "id"
    }
    
}

extension Actor: ImageRepresentable {
    public var path: String? {
        return image
    }
}
