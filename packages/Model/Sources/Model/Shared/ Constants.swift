//
//  File.swift
//  
//
//  Created by Bryan A Bolivar M on 11/11/22.
//

import Foundation

public struct Constants {
    public static var defaultImageURL: URL {
        guard let url = URL(string: "https://media.istockphoto.com/vectors/error-page-or-file-not-found-icon-vector-id924949200?k=20&m=924949200&s=170667a&w=0&h=-g01ME1udkojlHCZeoa1UnMkWZZppdIFHEKk6wMvxrs=")
        else {
            assertionFailure("this line should never be triggered")
            return URL(fileURLWithPath: "")
        }
        return url
    }
    
    public static let emptyDescriptionMessage = "No Description for this movie yet. please come back later."
}

