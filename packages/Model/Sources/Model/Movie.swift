//
//  Movie.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 20/10/22.
//

import Foundation
import UIKit

public struct Movie: Codable {
    
    public init(name: String, description: String, image: String? = nil, movieID: Int) {
        self.name = name
        self.description = description
        self.image = image
        self.movieID = movieID
    }
    
    public var dictionaryRepresentation: [String: String] {
        return [
            "name": name,
            "about": description,
            "imageURL": image ?? ""
        ]
    }
    
    /// The movie Title
    public let name: String
    
    /// The movie overview
    public let description: String
    
    //var genre: MovieGenre = .action
    
    /// The poster Path
    public let image: String?
    
    /// The movie id
    public let movieID: Int
    
    public enum CodingKeys: String, CodingKey {
        case name = "title"
        case description  = "overview"
        //case genre
        case image = "poster_path"
        case movieID = "id"
    }
    
    public func movieDetails() -> String {
        guard !description.isEmpty else {
            return Constants.emptyDescriptionMessage
        }
        return description
    }
    
}

extension Movie: ImageRepresentable {
    public var path: String? {
        return image
    }
}
