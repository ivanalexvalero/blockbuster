//
//  File.swift
//  
//
//  Created by Bryan A Bolivar M on 11/11/22.
//

import Foundation

import Foundation
import XCTest
@testable import Model

final class TrailerTests: XCTestCase {
    
    func testImageURLRepresentationShouldBeDefault() {
        let trailer = Trailer(name: "Rambo 4", image: "", url: "")
        XCTAssertEqual(trailer.imageURLRepresentation()?.absoluteURL, Constants.defaultImageURL.absoluteURL)
    }
    
    func testActorDictionaryRepresentationContainsAllKeys() {
        let actor = Actor(name: "Bryan", popularity: 1, id: 100)
        XCTAssertEqual(actor.dictionaryRepresentation.keys.count, 4)
    }
}
