//
//  ReviewTests.swift
//  
//
//  Created by Yanet Rodriguez on 12/11/2022.
//

import Foundation
import XCTest
@testable import Model

final class ReviewTests: XCTestCase {
    
    func testReviewsRightResponseInit() {
        let authorDetails = AuthorDetails(name: "", username: "", rating: 1)
        let results = Review(author: "", authorDetails: authorDetails, content: "", createdAt: "", id: "", updatedAt: "", url: "")
        let reviewsResponse = ReviewResponse(id: 99, page: 2, results: [results], totalPages: 2, totalResults: 4)
        
        XCTAssertEqual(reviewsResponse.id, 99)
        XCTAssertEqual(reviewsResponse.page, 2)
        XCTAssertEqual(reviewsResponse.results, [results])
        XCTAssertEqual(reviewsResponse.totalPages, 2)
        XCTAssertEqual(reviewsResponse.totalResults, 4)
    }
    
    func testAvatarNil() {
        let authorDetails = AuthorDetails(name: "Julle", username: "Jullyta", avatarPath: nil, rating: 4)
        XCTAssertNil(authorDetails.avatarPath)
    }
    
    func testAvatarRepresentationContainsAvatarURL() {
        let authorDetails = AuthorDetails(name: "Julle", username: "Jullyta", avatarPath: "avatar", rating: 4)
        XCTAssertEqual(authorDetails.imageURLRepresentation(), URL(string: "https://image.tmdb.org/t/p/w500/avatar"))
    }
    
    func testAvatarRepresentationNotPassingGuard() {
        let authorDetails = AuthorDetails(name: "Julle", username: "Jullyta", avatarPath: nil, rating: 4)
        XCTAssertEqual(authorDetails.imageURLRepresentation(), Constants.defaultImageURL)
    }
    
    func testAuthorDetailRightResponseInit() {
        let authorDetails = AuthorDetails(name: "Tom", username: "Holland", avatarPath: "https://media.revistagq.com/photos/61c0acf73a8adbe7b237d779/3:4/w_960,h_1280,c_limit/Tom%20Holland.jpg", rating: 5)
        XCTAssertEqual(authorDetails.name, "Tom")
        XCTAssertEqual(authorDetails.avatarPath, "https://media.revistagq.com/photos/61c0acf73a8adbe7b237d779/3:4/w_960,h_1280,c_limit/Tom%20Holland.jpg")
        XCTAssertEqual(authorDetails.username, "Holland")
        XCTAssertEqual(authorDetails.rating, 5)
    }
}
