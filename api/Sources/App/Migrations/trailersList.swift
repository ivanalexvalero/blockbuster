
import Foundation
import Fluent

final class trailerData : Codable {
    
    var name:String?
    var url:String?
    var image:String?
    
    init(name: String? = nil, url: String? = nil, image: String? = nil) {
        self.name = name
        self.url = url
        self.image = image
    }
   
}

func trailers() -> String {
    
    var list = [
        trailerData(name: "Game of Thrones", url: "https://www.imdb.com/title/tt0944947/?ref_=nv_sr_srsg_1",image: "https://m.media-amazon.com/images/M/MV5BOGY1NDQ1MDMtYTY1ZS00MGQ1LTg5ZTgtMDljM2IxY2NhODJhXkEyXkFqcGdeQXVyMTYzMDM0NTU@._V1_QL75_UY562_CR2,0,380,562_.jpg"),
        
        trailerData(name: "Halo", url: "https://www.imdb.com/title/tt2934286/?ref_=fn_al_tt_0",image: "https://m.media-amazon.com/images/M/MV5BYmY5YmJiM2QtNjdhOC00NjRhLTgyNDEtYmM1NmJhNjc5NDE2XkEyXkFqcGdeQXVyMjQ4ODcxNTM@._V1_QL75_UX380_CR0,0,380,562_.jpg"),
        
        trailerData(name: "La casa del dragón", url: "https://www.imdb.com/title/tt11198330/?ref_=hm_tpks_tt_t_5_pd_tp1_pbr_ic", image: "https://m.media-amazon.com/images/M/MV5BMTBiOTEyZDYtYTY4MS00NjA5LTgxY2EtMGRmZjQ5Y2E0ZDU3XkEyXkFqcGdeQXVyMTAyOTE2ODg0._V1_QL75_UY562_CR35,0,380,562_.jpg"),
        
        trailerData(name: "Breaking Bad", url: "https://www.imdb.com/title/tt0903747/?ref_=hm_tpks_tt_t_1_pd_tp1_pbr_ic", image: "https://m.media-amazon.com/images/M/MV5BNDkyZThhNmMtZDBjYS00NDBmLTlkMjgtNWM2ZWYzZDQxZWU1XkEyXkFqcGdeQXVyMTMzNDExODE5._V1_QL75_UX380_CR0,4,380,562_.jpg" ),
        
        trailerData(name: "El caballero oscuro", url: "https://www.imdb.com/title/tt0468569/?ref_=hm_tpks_tt_t_23_pd_tp1_pbr_ic", image: "https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_QL75_UX380_CR0,0,380,562_.jpg"),
        
        trailerData(name: "El señor de los anillos: Los anillos de poder", url: "https://www.imdb.com/title/tt7631058/?ref_=hm_stp_pvs_piv_tt_t_1", image: "https://m.media-amazon.com/images/M/MV5BNjIwZjQ5YTctZDg0ZS00YTI0LTk5YjEtYzVmYTJmYzRiNGUyXkEyXkFqcGdeQXVyNTI4MzE4MDU@._V1_QL75_UX380_CR0,0,380,562_.jpg"),
        
        trailerData(name: "Stranger Things", url: "https://www.imdb.com/title/tt4574334/?ref_=tt_tpks_tt_t_12_pd_tp1_pbr_ic", image: "https://m.media-amazon.com/images/M/MV5BOWI5ODc5MmYtYTU0Yi00MzVmLWEzZWItMWNiM2RiOTg0OGQzXkEyXkFqcGdeQXVyMTYzMDM0NTU@._V1_QL75_UX380_CR0,0,380,562_.jpg"),
        
        trailerData(name: "The Witcher", url: "https://www.imdb.com/title/tt5180504/?ref_=tt_tpks_tt_t_28_pd_tp1_pbr_ic", image: "https://m.media-amazon.com/images/M/MV5BN2FiOWU4YzYtMzZiOS00MzcyLTlkOGEtOTgwZmEwMzAxMzA3XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_QL75_UX380_CR0,0,380,562_.jpg"),
        
        trailerData(name: "The Peripheral", url: "https://www.imdb.com/title/tt8291284/?ref_=hm_stp_pvs_piv_tt_t_2", image: "https://m.media-amazon.com/images/M/MV5BZGM4OTdiMmMtYjBlMS00NmE2LTk1NTUtZGI0MWFhZmE3MzNlXkEyXkFqcGdeQXVyMTU2Mjg2NjE2._V1_QL75_UX380_CR0,2,380,562_.jpg")
    ]
    
    let js = JSONEncoder();
    js.outputFormatting = .withoutEscapingSlashes
    let json  = try? js.encode(list);
    let jsret =  String(data:json!,encoding: String.Encoding.utf8)
    
    
    return jsret!
    
}
