import Fluent
import Vapor
//userDefaults
func routes(_ app: Application) throws {

    app.post("login") { req -> String in
        sleep(3)
        let data = try req.content.decode(Login.self)
        if data.email == "user@email.com" && data.password == "12345678" {
            return "ok"
        } else {
            return "users not found or invalid credentials"
        }
    }
    
    app.get("trailers") { Request -> String in
        
        Request.headers.add(name: "Content-type", value: "Application/json")
        
        return trailers()
    }

    app.get { req async in
        "It works!"
    }

    app.get("hello") { req async -> String in
        "Hello, world!"
    }
    
    app.get("api", "movies") { req -> String in
        let user = Movie(name: "Black Adam",
                         poster: "https://cloudfront-us-east-1.images.arcpublishing.com/copesa/NKRTL7HORJFTVBRJ4MZ2SB5UNI.png",
                         genres: ["action", "fantasy"])
        let user2 = Movie(name: "Black Adam 2",
                          poster: "https://cloudfront-us-east-1.images.arcpublishing.com/copesa/NKRTL7HORJFTVBRJ4MZ2SB5UNI.png",
                          genres: ["action", "fantasy"])
        
        let response = [
            
            "movies": [user, user2]
        ]
        
        let jsonEncoder = JSONEncoder()
        jsonEncoder.outputFormatting = .withoutEscapingSlashes
        let json = try? jsonEncoder.encode(response)
        
        if let data = json, let str = String(data: data, encoding: .utf8) {
            return str
        }
        
        return ""
    }
    
    try app.register(collection: TodoController())
}

struct Login: Content {
    var email: String
    var password: String
}


final class Person: Content {
    let name: String
    let job: String
    let skills: [String]
    
    init(name: String, job: String, skills: [String]) {
        self.name = name
        self.job = job
        self.skills = skills
    }
}

enum Genre: Content {
    case horror
    case comedy
    case action
    case fantasy
}

final class Movie: Content {
    let name: String
    let poster: String
    let genres: [String]
    
    init(name: String, poster: String, genres: [String]) {
        self.name = name
        self.poster = poster
        self.genres = genres
    }
}
