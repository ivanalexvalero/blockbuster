//
//  AuthenticationViewControllerTests.swift
//  BlockbusterTests
//
//  Created by Yanet Rodriguez on 21/11/2022.
//

@testable import Blockbuster
import Foundation
import XCTest

class AuthenticationViewControllerTests: XCTestCase {
    
    // MARK: Subject under test
    
    var authViewController: AuthenticationViewController!
    var window: UIWindow!
    
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupAuthenticationViewController()
    }
    
    override func tearDown() {
        window = nil
        super.tearDown()
    }
    
    // MARK: Test setup
    
    func setupAuthenticationViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        authViewController = storyboard.instantiateViewController(withIdentifier: "AuthenticationViewController") as? AuthenticationViewController
    }
    
    func loadView() {
        window.addSubview(authViewController.view)
        RunLoop.current.run(until: Date())
    }
    
    // MARK: Test doubles
    
    class AuthenticationBusinessLogicSpy: AuthenticationBusinessLogic {
        var loginCalled = false
        
        func login(request: Authentication.Login.Request) {
            loginCalled = true
        }
    }
    
    // MARK: Tests
    
    func testShouldLoginWhenLoginButtonIsTapped() {
        // Given
        let auhtBusinessLogicSpy = AuthenticationBusinessLogicSpy()
        authViewController.interactor = auhtBusinessLogicSpy
        
        // When
        loadView()
        authViewController.loginButtonTapped(self)
        
        // Then
        XCTAssertTrue(auhtBusinessLogicSpy.loginCalled, "loginButtonTapped(_:) should ask the interactor to login")
    }
}
