//
//  AuthenticationWorkerTests.swift
//  BlockbusterTests
//
//  Created by Yanet Rodriguez on 21/11/2022.
//

@testable import Blockbuster
import Foundation
import XCTest

class AuthenticationWorkerTests: XCTestCase {
    // MARK: Subject under test
    
    var sut: AuthenticationWorker!
    
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupAuthenticationWorker()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: Test setup
    
    func setupAuthenticationWorker() {
        sut = AuthenticationWorker()
    }
    
    // MARK: Test doubles
    
    // MARK: Tests
    
    func testLoginOnSuccess()
    {
      // Given
      let email = "admin@admin.com"
      let password = "123456"
      
      // When
      var actualSuccess: Bool?
      let loginCompleted = expectation(description: "Wait for login to complete")
      sut.login(email: email, password: password) { (success) in
        actualSuccess = success
        loginCompleted.fulfill()
      }
      waitForExpectations(timeout: 5.0, handler: nil)
      
      // Then
      XCTAssertTrue(actualSuccess!, "login(email:password:completionHandler) should result in success")
    }
    
    func testLoginOnFailure() {
        // Given
        let email = "wrong"
        let password = "wrong"
        
        // When
        var actualSuccess: Bool?
        let loginCompleted = expectation(description: "Wait for login to complete")
        sut.login(email: email, password: password) { (success) in
            actualSuccess = success
            loginCompleted.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
        
        // Then
        XCTAssertFalse(actualSuccess!, "login(email:password:completionHandler) should result in failure")
    }
}

