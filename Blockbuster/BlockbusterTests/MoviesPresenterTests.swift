//
//  MoviesPresenterTests.swift
//  BlockbusterTests
//
//  Created by u633174 on 16/11/2022.
//

import XCTest
@testable import Blockbuster
import Model
import APIService

final class MoviesPresenterTests: XCTestCase {

    var sut: MoviesPresenter!
    var displayLogicSpy: MoviesDisplayLogicSpy!
    var movies: Movie!
    
    override func setUpWithError() throws {
        sut = MoviesPresenter()
        displayLogicSpy = MoviesDisplayLogicSpy()
        sut.viewController = displayLogicSpy
        movies = Movie(name: "name", description: "some", movieID: 2)
        
    }

    override func tearDownWithError() throws {
        sut = nil
        movies = nil
    }

    func testMovieDisplaySearch() {
        let response = Movies.Search.Response(response: MovieListResponse(page: 1, results: [movies]))
        
        sut.presenterMovieSearch(response: response)
        
        XCTAssertNotNil(sut)
        XCTAssertTrue(displayLogicSpy.displaySearchResultsCalled)
    }
    
    func testMovieDisplayLogicMovieList() {
        let response = Movies.MovieList.Response(response: MovieListResponse(page: 1, results: [movies]))
        
        sut.presentMovieList(response: response)
        
        XCTAssertEqual(displayLogicSpy.displayMovieRessultViewModel?.movies.first?.name, "name")
        XCTAssertTrue(displayLogicSpy.displayMovieResultsCalled)
    }
    
}

extension MoviesPresenterTests {
    class MoviesDisplayLogicSpy: MoviesDisplayLogic {
        var displaySearchResultsCalled = false
        var displaySearchRessultViewModel: Movies.Search.ViewModel?
        func displaySearchResults(viewModel: Blockbuster.Movies.Search.ViewModel) {
            displaySearchResultsCalled = true
            displaySearchRessultViewModel = viewModel
        }
        var displayMovieResultsCalled = false
        var displayMovieRessultViewModel: Movies.MovieList.ViewModel?
        func displayMovieListResult(viewModel: Blockbuster.Movies.MovieList.ViewModel) {
            displayMovieResultsCalled = true
            displayMovieRessultViewModel = viewModel
        }
    }
}


