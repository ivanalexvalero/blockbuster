//
//  MovieTableViewCell.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 20/10/22.
//

import UIKit
import Kingfisher
import Model

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    var movie: Movie? {
        didSet {
            guard let movie = movie else {
                return
            }
            
            nameLabel.text = movie.name
            //genreLabel.text = movie.genre.rawValue
            
            movieImageView.kf.setImage(with: movie.imageURLRepresentation())
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
