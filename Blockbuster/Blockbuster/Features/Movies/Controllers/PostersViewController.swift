//
//  PostersViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 24/10/22.
//

import UIKit
import Alamofire
import Model

class PostersViewController: UIViewController {
    var selectedMovie: Movie?
    var movies: [Movie] = []
    @IBOutlet weak var collectionView: UICollectionView!
    
    enum LayoutMode {
        case single
        case double
        case triple
    }
    
    var layoutMode: LayoutMode = .double
    
    private lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let cellWidth = (UIScreen.main.bounds.width * 0.333)
        let cellHeight = (UIScreen.main.bounds.width * 0.50)

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        layout.scrollDirection = .vertical
        return layout
    }()
    
    private lazy var collectionViewLayout2Cells: UICollectionViewFlowLayout = {
        let cellWidth = (UIScreen.main.bounds.width * 0.5)
        let cellHeight = (UIScreen.main.bounds.width * 0.75)

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        layout.scrollDirection = .vertical
        return layout
    }()
    
    private lazy var collectionViewLayout1Cell: UICollectionViewFlowLayout = {
        let cellWidth = UIScreen.main.bounds.width
        let cellHeight = UIScreen.main.bounds.height * 0.69

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        layout.scrollDirection = .vertical
        return layout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchMovies()
        title = "Posters"
        switchLayout()
        let barButtonItem = UIBarButtonItem(title: "Switch Layout", image: nil, target: self, action: #selector(switchLayout))
        navigationItem.rightBarButtonItem = barButtonItem
    }
    
    @objc func switchLayout() {
            switch self.layoutMode {
            case .single:
                self.layoutMode = .double
                self.collectionView.collectionViewLayout = self.collectionViewLayout2Cells
            case .double:
                self.layoutMode = .triple
                self.collectionView.collectionViewLayout = self.collectionViewLayout
            case .triple:
                self.layoutMode = .single
                self.collectionView.collectionViewLayout = self.collectionViewLayout1Cell
            }
    }
    
    @objc func fetchMovies() {
        let urlParams = [
            "api_key":"627e3f67dac2c73e662093fe66b67ce7",
        ]
        
        // Fetch Request
        AF.request("https://api.themoviedb.org/3/movie/popular", method: .get, parameters: urlParams)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: PopularMovie.self) { response in
                debugPrint(response)
                do {
                    guard let data = response.data else {
                        print("Data parsing failure, not found")
                        return
                    }
                    let movies = try JSONDecoder().decode(MovieListResponse.self, from: data)
                    self.movies = movies.results
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MovieDetailsSegue" {
            let vc = segue.destination as! MovieDetailsViewController
            vc.movie = selectedMovie
        }
    }

}

extension PostersViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PosterCellIdentifier", for: indexPath) as! PosterCollectionViewCell
        cell.movie = movies[indexPath.row]
        return cell
    }
}

extension PostersViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ReviewsViewController(movie: movies[indexPath.row])
        navigationController?.pushViewController(vc, animated: true)
    }
}
