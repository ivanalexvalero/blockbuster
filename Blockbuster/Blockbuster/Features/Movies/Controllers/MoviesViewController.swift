//
//  MoviesViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 20/10/22.
//

import UIKit
import Alamofire
import Model

protocol MoviesDisplayLogic: class {
    func displaySearchResults(viewModel: Movies.Search.ViewModel)
    func displayMovieListResult(viewModel: Movies.MovieList.ViewModel)
}

class MoviesViewController: UIViewController, MoviesDisplayLogic {
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    var selectedMovie: Movie?
    let viewModel = MoviesViewModel()
    var interactor: MoviesBusinessLogic?
    var router: (NSObjectProtocol & MoviesRoutingLogic & MoviesDataPassing)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        fetchMovies()
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(fetchMovies), for: .valueChanged)
        
        tableView.keyboardDismissMode = .onDrag
        searchTextField.delegate = self
        viewModel.delegate = self
        setup()
    }
    
    private func setup() {
      let viewController = self
      let interactor = MoviesInteractor()
      let presenter = MoviesPresenter()
      let router = MoviesRouter()
      viewController.interactor = interactor
      viewController.router = router
      interactor.presenter = presenter
      presenter.viewController = viewController
      router.viewController = viewController
      router.dataStore = interactor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = viewModel.titleForHome
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchMovieList()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MovieDetailsSegue" {
            let vc = segue.destination as! MovieDetailsViewController
            vc.movie = selectedMovie
            vc.viewModel = viewModel
        }
    }
    
    @objc func fetchMovies() {
        refreshControl.endRefreshing()
        searchTextField.text = ""
        searchTextField.endEditing(true)
        
        viewModel.fetchMovies {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } failure: {
            
        }
    }
    
    @IBAction func searchMovie(_ sender: Any) {
        guard
            let movieName = searchTextField.text,
            !movieName.isEmpty
        else { return }
        
        let request = Movies.Search.Request(query: movieName)
        interactor?.searchMovie(request: request)
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        print("textChanged")
        searchMovie(self)
    }
    
    
    @IBAction func segmentControlChanged(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            viewModel.isFavoritesEnabled = false
            tableView.reloadData()
        } else {
            viewModel.isFavoritesEnabled = true
            tableView.reloadData()
        }
    }
    
    func fetchMovieList() {
        let request = Movies.MovieList.Request()
        interactor?.fetchMovieList(request: request)
    }
    
    func displaySearchResults(viewModel: Movies.Search.ViewModel) {
        self.viewModel.updateMoviesList(newList: viewModel.movies)
    }
    func displayMovieListResult(viewModel: Movies.MovieList.ViewModel) {
        self.viewModel.movieList(viewModel.movies)
    }
    
}

extension MoviesViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCellIdentifier", for: indexPath) as? MovieTableViewCell
        else {
            return UITableViewCell()
        }
        let movie = viewModel.movie(indexPath.row)
        cell.movie =  movie
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalMovies()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension MoviesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie =  viewModel.movie(indexPath.row)
        performSegue(withIdentifier: "MovieDetailsSegue", sender: self)
    }
}

extension MoviesViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")

    }
}


extension MoviesViewController: MoviesViewModelDelegate {
    func didFetchMovies() {
        self.tableView.reloadData()
    }
    
    func willFetchMovies() {
        
    }
    
    func fetchDidFail() {
        //let alert = UIAlertController(title: "Failed", message: "Try again", preferredStyle: .alert)
    }
}

/*
 bloquear busqueda hasta que hayan pasado 3 segundos despues de la ultima edicion
 TIP: DispatchAfter + State
 */
