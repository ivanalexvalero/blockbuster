//
//  MoviesListViewModel.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 24/10/22.
//

import Foundation
import Alamofire
import SystemDesign
import APIService
import Moya
import UIKit
import Model

protocol MoviesViewModelDelegate {
    func didFetchMovies()
    func willFetchMovies()
    func fetchDidFail()
}

struct MovieListResponse: Codable {
    let page: Int
    let results: [Movie]
}

class MoviesViewModel {
    private var movies: [Movie] = []
    var delegate: MoviesViewModelDelegate?
    
    let urlBase = "https://api.themoviedb.org/3/"
    
    enum EndPoint: String {
        case moviesPopular = "movie/popular"
        case searchMovie = "search/movie"
    }
    
    var isFavoritesEnabled: Bool = false
    
    var titleForHome: String {
        guard let email = UserDefaults.standard.string(forKey: "email") else {
            return "Welcome anonymous"
        }
        
        return email
    }
    
    func updateMoviesList(newList: [Movie]) {
        self.movies = newList
        delegate?.didFetchMovies()
    }
    
    func movieList(_ movie: [Movie]) {
        self.movies = movie
        delegate?.didFetchMovies()
    }
    
    func moviesDataSource() -> [Movie] {
        if isFavoritesEnabled {
            return fetchFavorites()
        } else {
            return movies
        }
        
    }
    
    func updateMovieList(_ newMedia: [Movie]) {
        self.movies = newMedia
        delegate?.didFetchMovies()
    }
    
    func movie(_ index: Int) -> Movie? {
        if isFavoritesEnabled {
            return fetchFavorites()[index]
        } else {
            return movies[index]
        }
    }
    
    func totalMovies() -> Int {
        if isFavoritesEnabled {
            return fetchFavorites().count
        } else {
            return movies.count
        }
    }
    
    @objc func fetchMovies(completion: @escaping () -> (), failure: @escaping () -> ()) {
        let provider = MoyaProvider<TheMovieDBAPI>()
        provider.request(.movieslist) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let movies = try JSONDecoder().decode(MovieListResponse.self, from: moyaResponse.data)
                    self.movies = movies.results
                    completion()
                } catch {
                    print(error.localizedDescription)
                    failure()
                }
            case let .failure(error):
                print(error.localizedDescription)
                failure()
            }
        }
    }
    
    @objc func fetchMovies() {
        delegate?.willFetchMovies()
        let urlParams = [
            "api_key":"627e3f67dac2c73e662093fe66b67ce7",
        ]
        
        // Fetch Request
        AF.request(urlBase + EndPoint.moviesPopular.rawValue, method: .get, parameters: urlParams)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: PopularMovie.self) { response in
                debugPrint(response)
                do {
                    guard let data = response.data else {
                        print("Data parsing failure, not found")
                        return
                    }
                    let movies = try JSONDecoder().decode(MovieListResponse.self, from: data)
                    self.movies = movies.results
                    DispatchQueue.main.async {
                        self.delegate?.didFetchMovies()
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
    }
    
    func saveMovieAsFavorite(_ movie: Movie) {
        DatabaseManager.shared.save(entity: .movie, parameters: movie.dictionaryRepresentation)
    }
    
    func fetchFavorites() -> [Movie] {
        let movies = DatabaseManager.shared.fetch(entity: .movie, queryType: .all).map({ object -> Movie in
            Movie(name: object.value(forKey: "name") as! String,
                  description: object.value(forKey: "about") as! String,
                  image: object.value(forKey: "imageURL") as? String,
                  movieID: object.value(forKey: "id") as! Int)
        })
        return movies
    }
    
    func deleteMovie(_ movie: Movie) {
        DatabaseManager.shared.delete(entity: .movie, value: movie.name)
    }
    
    func isFavoriteMovie(_ movie: Movie) -> Bool {
        return fetchFavorites().contains { $0.name == movie.name }
    }
}

struct PopularMovie: Decodable {
    let url: String
}
