//
//  TrailersViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 28/10/22.
//

import UIKit
import SnapKit
import SystemDesign
import APIService
import Moya
import Model

class TrailersViewController: UIViewController {

    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.title = "Trailers"
        
        let provider = MoyaProvider<AuthenticatorAPI>()
        provider.request(.trailers) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let trailers = try JSONDecoder().decode([Trailer].self, from: moyaResponse.data)
                    print(trailers)
                    if !trailers.isEmpty {
                        self.addTrailerView(trailer: trailers.first!)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            case let .failure(error):
                print(error.localizedDescription)
                
            }
        }
    }
    
    func addTrailerView(trailer: Trailer) {
        let movieCard = TrailerView.viewRepresentation(url: trailer.url,
                                                       name: trailer.name,
                                                       image: trailer.image,
                                                       viewController: self) { url in
            
            guard let urlRepresentation = URL(string: url) else {
                return
            }
            
            UIApplication.shared.open(urlRepresentation)
        }
        view.addSubview(movieCard.view)
        
        movieCard.view.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.width.equalToSuperview()
        }
    }
}
