//
//  SubscribeViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 16/11/22.
//  
//

import UIKit

class SubscribeViewController: UIViewController {
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Properties
    var presenter: ViewToPresenterSubscribeProtocol?
    
    
    func requestSubscription() {
        presenter?.requestSubscription(with: SubscriptionData(name: "Bryan", cardNumber: 10, expireMonth: 12, expireYear: 2023))
    }
    
}

extension SubscribeViewController: PresenterToViewSubscribeProtocol{
    
    // TODO: Implement View Output Methods
    
    func showSubscriptionDetails(status: Bool) {
        
    }
}
