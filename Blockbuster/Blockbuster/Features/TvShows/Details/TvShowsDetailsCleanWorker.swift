import UIKit
import Model

class TvShowsDetailsCleanWorker {
    
    var presenter: TvShowsDetailsCleanPresenter?
    
    func getTvShowsFavorites() -> [TvShow] {
        let tvShows = DatabaseManager.shared.fetch(entity: .tvShow, queryType: .all).map({ object -> TvShow in
            TvShow(name: object.value(forKey: "name") as! String,
                   description: object.value(forKey: "about") as! String,
                   image: object.value(forKey: "imageURL") as? String,
                   rating: (object.value(forKey: "rating") as! NSString).doubleValue)
        })
        return tvShows
    }
    
    func saveTvShowAsFavorite(_ tvshow: TvShow){
        presenter = TvShowsDetailsCleanPresenter()
        if isFavoriteTvShow(tvshow){
            return
        }else{
            DatabaseManager.shared.save(entity: .tvShow, parameters: tvshow.dictionaryRepresentation)
        }
    }
    
    func deleteTvShow(_ tvshow: TvShow){
        DatabaseManager.shared.delete2(entity: .tvShow, value: tvshow.name)
    }
    
    func isFavoriteTvShow(_ tvshow: TvShow) -> Bool {
        return getTvShowsFavorites().contains { $0.name == tvshow.name }
    }
    
}
