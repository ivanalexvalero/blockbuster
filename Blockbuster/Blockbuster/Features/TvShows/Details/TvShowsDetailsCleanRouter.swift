import UIKit

@objc protocol TvShowsDetailsCleanRoutingLogic{}

protocol TvShowsDetailsCleanDataPassing
{
  var dataStore: TvShowsDetailsCleanDataStore? { get }
}

class TvShowsDetailsCleanRouter: NSObject, TvShowsDetailsCleanRoutingLogic, TvShowsDetailsCleanDataPassing
{
  weak var viewController: TvShowsDetailsCleanViewController?
  var dataStore: TvShowsDetailsCleanDataStore?
}
