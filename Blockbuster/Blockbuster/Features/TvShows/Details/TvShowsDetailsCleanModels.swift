import UIKit
import Model

enum TvShowsDetailsClean
{
    // MARK: Use cases
    
    enum GetTvShowDetails
    {
        struct Request{}
        struct Response {}
        struct ViewModel
        {
            var selectedTvShow: TvShow
            var isFavorite: Bool
        }
    }
}
