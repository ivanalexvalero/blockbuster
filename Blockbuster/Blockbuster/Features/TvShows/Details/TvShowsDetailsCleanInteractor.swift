import UIKit
import Model

protocol TvShowsDetailsCleanBusinessLogic
{
    func displayTvShowsDetails()
    func saveTvShowToFavorites(tvShow: TvShow)
    func deleteTvShowFromFavorites(tvShow: TvShow)
}

protocol TvShowsDetailsCleanDataStore
{
    var selectedTvShow: TvShow! {get set}
}

class TvShowsDetailsCleanInteractor: TvShowsDetailsCleanBusinessLogic, TvShowsDetailsCleanDataStore
{
    var selectedTvShow: TvShow!
    var presenter: TvShowsDetailsCleanPresentationLogic?
    var worker = TvShowsDetailsCleanWorker()
    
    // MARK: Do something
    
    func displayTvShowsDetails() {
        let isFavorite = worker.isFavoriteTvShow(self.selectedTvShow)
        presenter?.presentTvShowDetails(tvshow: self.selectedTvShow, isFavorite: isFavorite)
    }
    
    func saveTvShowToFavorites(tvShow: TvShow) {
        worker.saveTvShowAsFavorite(tvShow)
    }
    
    func deleteTvShowFromFavorites(tvShow: TvShow) {
        worker.deleteTvShow(tvShow)
    }
}
