import UIKit
import Model

protocol TvShowsDetailsCleanDisplayLogic: AnyObject {
    func displayTvShowDetails(viewModel: TvShowsDetailsClean.GetTvShowDetails.ViewModel)
}

class TvShowsDetailsCleanViewController: UIViewController, TvShowsDetailsCleanDisplayLogic
{
    @IBOutlet weak var tvPosterImage: UIImageView!
    @IBOutlet weak var tvDescriptionLabel: UILabel!
    @IBOutlet weak var tvNameLabel: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    var interactor: TvShowsDetailsCleanBusinessLogic?
    var router: (NSObjectProtocol & TvShowsDetailsCleanRoutingLogic & TvShowsDetailsCleanDataPassing)?
    var selectedTvShow: TvShow?
    var isFavorite: Bool = false
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = TvShowsDetailsCleanInteractor()
        let presenter = TvShowsDetailsCleanPresenter()
        let router = TvShowsDetailsCleanRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: View lifecycle
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        interactor?.displayTvShowsDetails()
        
    }
    
    func displayTvShowDetails(viewModel: TvShowsDetailsClean.GetTvShowDetails.ViewModel) {
        self.selectedTvShow = viewModel.selectedTvShow
        self.isFavorite = viewModel.isFavorite
        configureUI()
    }
    
    func configureUI(){
        tvNameLabel.text = selectedTvShow?.name
        tvDescriptionLabel.text = selectedTvShow?.description
        guard let url = selectedTvShow?.imageURLRepresentation() else {
            return
        }
        tvPosterImage.kf.setImage(with: url)
        
        isFavorite
        ? favoriteBtn.setTitle("Unfavorite", for: .normal)
        : favoriteBtn.setTitle("Add to favs", for: .normal)
    }
    
    
    // MARK: Save/Delete Favorite
    @IBAction func favoritesBtnPressed(_ sender: Any) {
        isFavorite
        ? interactor?.deleteTvShowFromFavorites(tvShow: self.selectedTvShow!)
        : interactor?.saveTvShowToFavorites(tvShow: self.selectedTvShow! )
    }
    
}
