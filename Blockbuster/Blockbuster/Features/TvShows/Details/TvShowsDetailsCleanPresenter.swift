import UIKit
import Model

protocol TvShowsDetailsCleanPresentationLogic
{
    func presentTvShowDetails(tvshow: TvShow, isFavorite: Bool)
}

class TvShowsDetailsCleanPresenter: TvShowsDetailsCleanPresentationLogic
{
    weak var viewController: TvShowsDetailsCleanViewController?
    var interactor = TvShowsDetailsCleanInteractor()
    
    func presentTvShowDetails(tvshow tvShow: TvShow, isFavorite: Bool) {
        let viewModel = TvShowsDetailsClean.GetTvShowDetails.ViewModel(selectedTvShow: tvShow, isFavorite: isFavorite)
        viewController?.displayTvShowDetails(viewModel: viewModel)
    }
}
