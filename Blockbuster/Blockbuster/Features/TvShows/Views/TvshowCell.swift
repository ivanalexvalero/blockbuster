//
//  TvshowCell.swift
//  Blockbuster
//
//  Created by Julian Martinez on 27/10/2022.
//

import UIKit
import SnapKit
import Kingfisher
import Model

class TvshowCell: UITableViewCell {
    
    
    static let identifier: String = "tvshowcellIdentifier"
    
    // MARK: - UI Elements
    lazy var tvshowImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.cornerRadius = 40
        image.clipsToBounds = true
        return image
    }()
    
    lazy var tvshowTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
        label.textColor = .black
        label.numberOfLines = 1
        return label
    }()
    
    lazy var tvshowRating: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 14)
        label.textColor = .black
        label.numberOfLines = 1
        return label
    }()
    
    
    var tvshow: TvShow? {
        didSet {
            guard let tvshow = tvshow else {
                return
            }
            tvshowTitle.text = tvshow.name
            tvshowRating.text = "Rating: ⭐ \(tvshow.rating)"
            if let url = tvshow.imageURLRepresentation() {
                tvshowImage.kf.setImage(with: url)
            }
        }
    }
    
    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setupUI()
    }
    
}


// MARK: - UI Setup Constraints

extension TvshowCell {
    private func setupUI() {
        contentView.addSubview(tvshowImage)
        contentView.addSubview(tvshowTitle)
        contentView.addSubview(tvshowRating)
        
        
        tvshowImage.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(10)
            make.width.equalTo(80)
            make.height.equalTo(80)
        }
        
        tvshowTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalTo(tvshowImage.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(-10)
        }
        
        tvshowRating.snp.makeConstraints { make in
            make.top.equalTo(tvshowTitle.snp.bottom).offset(10)
            make.leading.equalTo(tvshowImage.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(-10)
        }
        
    }
}
