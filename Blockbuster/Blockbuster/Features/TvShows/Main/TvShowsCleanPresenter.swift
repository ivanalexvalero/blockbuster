import UIKit

protocol TvShowsCleanPresentationLogic {
    func presentTvShowsLit(response: TvShowsClean.FetchTvShowsList.Response)
    func presentFavoritesTvShowList(response: TvShowsClean.FetchFavoritesTvShowsList.Response)
}

class TvShowsCleanPresenter: TvShowsCleanPresentationLogic {
    weak var viewController: TvShowsCleanDisplayLogic?
    
    func presentTvShowsLit(response: TvShowsClean.FetchTvShowsList.Response) {
        let viewModel = TvShowsClean.FetchTvShowsList.ViewModel(tvshows: response.tvshows)
        viewController?.displayTvShows(viewModel: viewModel)
    }
    
    func presentFavoritesTvShowList(response: TvShowsClean.FetchFavoritesTvShowsList.Response) {
        let viewModel = TvShowsClean.FetchFavoritesTvShowsList.ViewModel(tvshows: response.tvshows)
        viewController?.displayFavoritesTvShowsList(viewModel: viewModel)
    }
}
