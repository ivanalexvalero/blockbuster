import UIKit
import Model

enum TvShowsClean
{
    enum FetchTvShowsList
    {
        struct Request {}
        struct Response {
            let tvshows: [TvShow]
        }
        struct ViewModel {
            let tvshows: [TvShow]
        }
    }
    
    enum FetchFavoritesTvShowsList{
        struct Request {}
        struct Response{
            let tvshows: [TvShow]
        }
        struct ViewModel {
            let tvshows: [TvShow]
        }
    }
}
