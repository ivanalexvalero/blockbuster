import UIKit
import Model

protocol TvShowsCleanDisplayLogic: AnyObject {
    func displayTvShows(viewModel: TvShowsClean.FetchTvShowsList.ViewModel)
    func displayFavoritesTvShowsList(viewModel: TvShowsClean.FetchFavoritesTvShowsList.ViewModel)
}

class TvShowsCleanViewController: UIViewController, TvShowsCleanDisplayLogic {
    //MARK: Properties
    var interactor: TvShowsCleanBusinessLogic?
    var router: (NSObjectProtocol & TvShowsCleanRoutingLogic & TvShowsCleanDataPassing)?
    
    var tvShows = [TvShow]()
    var selectedShow: TvShow?
    
    let tableView = UITableView()
    let refreshControl = UIRefreshControl()
    lazy var segmentedControl: UISegmentedControl = {
        let control = UISegmentedControl(items: ["Tv Shows","Favorites"])
        control.selectedSegmentIndex = 0
        control.addTarget(self, action: #selector(segmentedControlChanged(_:)), for: .valueChanged)
        return control
    }()
    
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    private func setup() {
        let viewController = self
        let interactor = TvShowsCleanInteractor()
        let presenter = TvShowsCleanPresenter()
        let router = TvShowsCleanRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTvShowsList()
        configureSegmentedControl()
        configureTableView()
    }
    
    func loadTvShowsList()  {
        let request = TvShowsClean.FetchTvShowsList.Request()
        interactor?.fetchTvShowsList(request: request)
    }
    func loadFavoritesTvShowsList(){
        let request = TvShowsClean.FetchFavoritesTvShowsList.Request()
        interactor?.fetchFavoritesTvShowsList(request: request)
    }
    
    func displayTvShows(viewModel: TvShowsClean.FetchTvShowsList.ViewModel) {
        tvShows = viewModel.tvshows
        self.tableView.reloadData()
    }
    
    func displayFavoritesTvShowsList(viewModel: TvShowsClean.FetchFavoritesTvShowsList.ViewModel) {
        tvShows = viewModel.tvshows
        self.tableView.reloadData()
    }
    
    //MARK: - Configure TableView
    func configureTableView(){
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.addSubview(refreshControl)
        view.addSubview(tableView)
        tableView.estimatedRowHeight = 100
        tableView.separatorStyle = .singleLine
        tableView.allowsSelection = true
        tableView.keyboardDismissMode = .onDrag
        tableView.snp.makeConstraints { make in
            //            make.top.equalTo(view.layoutMargins.top).offset(10)
            make.top.equalTo(segmentedControl.snp.bottom).offset(10)
            make.leading.equalTo(view.layoutMargins.left)
            make.trailing.equalTo(view.layoutMargins.right)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
        tableView.register(TvshowCell.self, forCellReuseIdentifier: TvshowCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func configureSegmentedControl(){
        view.addSubview(segmentedControl)
        
        segmentedControl.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).multipliedBy(1.1)
            make.leading.equalTo(view.layoutMargins.left).offset(50)
            make.trailing.equalTo(view.layoutMargins.right).offset(-50)
            make.height.equalTo(40)
        }
    }
    
    @objc func segmentedControlChanged(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0 {
            loadTvShowsList()
        } else {
            loadFavoritesTvShowsList()
        }
    }
}

extension TvShowsCleanViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tvShows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: TvshowCell.identifier, for: indexPath) as? TvshowCell
        else {
            return UITableViewCell()
        }
        
        let tvshow = self.tvShows[indexPath.row]
        cell.tvshow = tvshow
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension TvShowsCleanViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedShow = self.tvShows[indexPath.row]
        interactor?.getSelectedTvShow(selectedShow!)
        router?.routeToTvShowDetail(segue: nil)
    }
}
