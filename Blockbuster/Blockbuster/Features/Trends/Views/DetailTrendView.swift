//
//  DetailTrendView.swift
//  Blockbuster
//
//  Created by Nicolas Pepe on 31/10/2022.
//

import SwiftUI
import Kingfisher
import Model

public struct DetailTrendView: View {
    @State var name: String = ""
    @State var imagen: String
    var viewModel = TrendsViewModel()
    var media: Media?
    
   public var body: some View {
        VStack {
            Spacer()
            VStack(alignment: .leading, spacing: 11) {
                Text(name)
                    .font(.system(size: 24, weight: .medium, design: .default))
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .clipped()
                Text("Genre")
                    .font(.headline.weight(.medium))
                    .foregroundColor(.white)
                Button(action: {
                    print("Guardar en favorito")
                    let media = Media(name: name, title: name, description: "", image: imagen)
                    viewModel.saveTrendAsFavorite(trend: media)
                }, label: {
                    Text("Save favorite").font(.system(.headline, design: .serif))
                        .padding(.vertical, 12)
                        .padding(.horizontal, 15)
                        .background(.green)
                        .foregroundColor(.black)
                        .mask { RoundedRectangle(cornerRadius: 12, style: .continuous) }
                        .padding(.top, 8)
                })
                Text("View trailer")
                    .font(.system(.headline, design: .serif))
                    .padding(.vertical, 12)
                    .padding(.horizontal, 15)
                    .background(.white)
                    .foregroundColor(.black)
                    .mask { RoundedRectangle(cornerRadius: 12, style: .continuous) }
                    .padding(.top, 8)
            }
            .padding(.horizontal, 24)
            Spacer()
                .frame(height: 100)
                .clipped()
        }
        .background {
            
            KFImage(URL(string: "https://image.tmdb.org/t/p/w500"+imagen)!)
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fill)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .clipped()
    }
}

extension DetailTrendView {
    public static func viewRepresentation(
     name: String, imagen: String,
    viewController: UIViewController) -> UIHostingController<DetailTrendView> {
            
            let childView = UIHostingController(rootView: DetailTrendView(name: name,imagen: imagen))
        viewController.view.addSubview(childView.view)
        childView.didMove(toParent: viewController)
        return childView
    }
}
