//
//  TrendsTableViewCell.swift
//  Blockbuster
//
//  Created by u633168 on 26/10/2022.
//

import UIKit
import Kingfisher
import Model

class TrendsTableViewCell: UITableViewCell {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    var movie: Media? {
        didSet {
            guard let movie = movie else {
                return
            }
            
             if movie.name == nil {
                 nameLabel.text = movie.title
            }else{
                nameLabel.text = movie.name
            }
            
            if let url = movie.imageURLRepresentation() {
                movieImageView.kf.setImage(with: url)
            } else {
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
