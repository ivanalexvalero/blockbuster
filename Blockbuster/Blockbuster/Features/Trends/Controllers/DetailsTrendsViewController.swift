//
//  DetailsTrendsViewController.swift
//  Blockbuster
//
//  Created by Nicolas Pepe on 31/10/2022.
//

import UIKit
import SnapKit
import Model



class DetailsTrendsViewController: UIViewController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var trends: Media?
    var trend: Media?
    var viewModel: TrendsViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let trends = trends else {return}
        
        
        if trends.name == nil {
            let movieCard = DetailTrendView.viewRepresentation( name: trends.title!, imagen: trends.image! ,viewController: self)
            view.addSubview(movieCard.view)
            
            movieCard.view.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.height.width.equalToSuperview()
            }
           
        }else{
            let movieCard = DetailTrendView.viewRepresentation( name: trends.name!, imagen: trends.image! ,viewController: self)
            view.addSubview(movieCard.view)
            
            movieCard.view.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.height.width.equalToSuperview()
                make.size.equalToSuperview()
            }
        }
    }
}
