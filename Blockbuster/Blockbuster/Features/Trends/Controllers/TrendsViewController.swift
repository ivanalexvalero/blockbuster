//
//  TrendsViewController.swift
//  Blockbuster
//
//  Created by u633168 on 26/10/2022.
//

import UIKit
import Alamofire
import Model

struct TrendsListResponse: Codable {
    let page: Int
    let results: [Media]
}

protocol TrendsDisplayLogic: class
{
  func displaySomething(viewModel: Trends.MediaList.ViewModel)
}

class TrendsViewController: UIViewController, TrendsDisplayLogic {
    
    
    @IBOutlet weak var tableView: UITableView!
    var interactor: TrendsBusinessLogic?
    var router: (NSObjectProtocol & TrendsRoutingLogic & TrendsDataPassing)?
    
    
    let refreshControl = UIRefreshControl()
    var selectedMovie: Movie?
    let viewModel = TrendsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        //fetchMovies()
        tableView.addSubview(refreshControl)
        //refreshControl.addTarget(self, action: #selector(fetchMovies), for: .valueChanged)
        
        tableView.keyboardDismissMode = .onDrag
        
        viewModel.delegate = self
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Trends"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        doSomething()
    }
    
    private func setup()
    {
        let viewController = self
        let interactor = TrendsInteractor()
        let presenter = TrendsPresenter()
        let router = TrendsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    
    @objc func fetchMovies() {
        refreshControl.endRefreshing()
        
        viewModel.fetchMovies {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } failure: {
            
        }
        
    }
    
    func doSomething()
    {
      let request = Trends.MediaList.Request()
      interactor?.doSomething(request: request)
    }
    
    func displaySomething(viewModel: Trends.MediaList.ViewModel)
    {
        self.viewModel.updateMediaList(viewModel.results)
    }
}

extension TrendsViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrendsCellIdentifier", for: indexPath) as? TrendsTableViewCell
        else {
            return UITableViewCell()
        }
        let movie = viewModel.movies[indexPath.row]
        cell.movie = movie
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.movies.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
extension TrendsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Al hacer click enviar a Details Trends
       
    }
}


extension TrendsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailsTrendsViewController()
        vc.trends = viewModel.movies[indexPath.row]
        let nca = UINavigationController(rootViewController: vc)
       
        
        self.present(nca, animated: true, completion: nil )
    }
}

extension TrendsViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
}


extension TrendsViewController: TrendsViewModelDelegate {
    func didFetchMovies() {
        self.tableView.reloadData()
    }
    
    func willFetchMovies() {
        
    }
    
    func fetchDidFail() {
        _ = UIAlertController(title: "Failed", message: "Try again", preferredStyle: .alert)
    }
}
 

