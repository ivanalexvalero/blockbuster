//
//  TrendsModels.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 15/11/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import Model

enum Trends
{
  // MARK: Use cases
  
  enum MediaList
  {
    struct Request { }
      
    struct Response {
        var response: TrendsListResponse
    }
      
    struct ViewModel {
        var results: [Media]
    }
  }
}
