//
//  TrendsViewModel.swift
//  Blockbuster
//
//  Created by u633168 on 26/10/2022.
//

import Foundation
import Alamofire
import Moya
import APIService
import CoreData
import UIKit
import Model

protocol TrendsViewModelDelegate {
    func didFetchMovies()
    func willFetchMovies()
    func fetchDidFail()
}

class TrendsViewModel {
    var movies: [Media] = []
    var delegate: TrendsViewModelDelegate?
    
    @objc func fetchMovies(completion: @escaping () -> (), failure: @escaping () -> ()) {
        let provider = MoyaProvider<TheMovieDBAPI>()
        provider.request(.movieTrends) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                   
                   let movies = try JSONDecoder().decode(TrendsListResponse.self, from: moyaResponse.data)
                   
         
           self.movies = movies.results
                    completion()
                } catch {
                    print(error.localizedDescription)
                    failure()
                }
            case let .failure(error):
                print(error.localizedDescription)
                failure()
            }
        }
    }
    
    func updateMediaList(_ newMedia: [Media]) {
        self.movies = newMedia
        delegate?.didFetchMovies()
    }
    
    func saveTrendAsFavorite(trend: Media) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "MovieEntity", in: managedContext)!
        let movieObject = NSManagedObject(entity: entity, insertInto: managedContext)
        
        movieObject.setValue(trend.name, forKey: "name")
        movieObject.setValue(trend.image ?? "", forKey: "imageURL")
        movieObject.setValue(trend.description, forKey: "about")
        
        do {
            try managedContext.save()
        } catch {
            print(error.localizedDescription)
        }
    }
}

