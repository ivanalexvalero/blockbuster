//
//  ActorViewModel.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 26/10/2022.
//

import Foundation
import Alamofire
import SystemDesign
import APIService
import Moya
import UIKit
import Model

protocol ActorsViewModelDelegate {
    func didFetchActors()
    func willFetchActor()
    func fetchDidFail()
}

class ActorViewModel {
    
    var actors : [Actor] = []
    var delegate: ActorsViewModelDelegate?
    var isFavoritesEnabled: Bool = false
    var loader: ActorLoader
    
    
    init(loader: ActorLoader) {
        self.loader = loader
    }
    
    func actorsDataSource() -> [Actor]{
        return actors
    }
    
    func actor(_ index: Int) -> Actor? {
        return actors[index]
    }
    
    func totalActors() -> Int {
        return actors.count
    }
    
    func saveActorAsFavorite(_ actor: Actor) {
        DatabaseManager.shared.save(entity: .actor, parameters: actor.dictionaryRepresentation)
    }
    
    func fetchFavorites() -> [Actor] {
        let actors = DatabaseManager.shared.fetch(entity: .actor, queryType: .all).map({ object -> Actor in
            Actor(name: object.value(forKey: "name") as! String,
                  popularity: object.value(forKey: "popularity") as! Double,
                  id: object.value(forKey: "id") as! Int,
                  image: object.value(forKey: "imageURL") as? String)
        })
        return actors
    }
    
    func fetchActors(completion: @escaping () -> (), failure: @escaping () -> ()){
        
//        let provider = MoyaProvider<TheMovieDBAPI>()
//        provider.request(.actorpopular) { result in
//            switch result {
//            case let .success(moyaResponse):
//                do {
//                    print(result)
//                    let actors = try JSONDecoder().decode(ActorListResponse.self, from: moyaResponse.data)
//                    self.actors = actors.results
//                    completion()
//                } catch {
//                    print(error)
//                    failure()
//                }
//            case let .failure(error):
//                print("Error Actores: \(error)")
//                print(error.localizedDescription)
//                failure()
//            }
//        }
        loader.loadActor(completion: { actors in
            self.actors = actors
            completion()
        })
    }
    
    func deleteActor(_ actor: Actor) {
        DatabaseManager.shared.delete2(entity: .actor, value: actor.name)
    }
    
    func isFavoriteActor(_ actor: Actor) -> Bool {
        return fetchFavorites().contains { $0.id == actor.id }
    }
}

struct PopularActor: Decodable {
    let url: String
}
protocol ActorLoader {
    func loadActor(completion: @escaping ([Actor]) -> ())
}

class APIActorLoader: ActorLoader {
    
    func loadActor(completion: @escaping ([Actor]) -> ()) {
        let urlParams = [
            "api_key":"627e3f67dac2c73e662093fe66b67ce7",
        ]
        
        // Fetch Request
        AF.request("https://api.themoviedb.org/3/person/popular", method: .get, parameters: urlParams)
            .validate(statusCode: 200..<300)
            .responseDecodable(of: PopularActor.self) { response in
                debugPrint(response)
                do {
                    guard let data = response.data else {
                        print("Data parsing failure, not found")
                        return
                    }
                    let actors = try JSONDecoder().decode(ActorListResponse.self, from: data)
                    completion(actors.results)
                } catch {
                    print(error.localizedDescription)
                }
            }
    }
}

class CoreDataActorLoader: ActorLoader {
    func loadActor(completion: @escaping ([Actor]) -> ()) {
        let actors = DatabaseManager.shared.fetch(entity: .actor, queryType: .all).map({ object -> Actor in
            Actor(name: object.value(forKey: "name") as! String,
                  popularity: object.value(forKey: "popularity") as! Double,
                  id: object.value(forKey: "id") as! Int,
                  image: object.value(forKey: "imageURL") as? String)
        })
        completion(actors)
    }
}

class JSONActorsLoader: ActorLoader {
    var fm = FileManager.default
    var mainUrl: URL? = Bundle.main.url(forResource: "jsonActors", withExtension: "json")
    var actors : [Actor] = []
        
    func loadActor(completion: @escaping ([Actor]) -> ()) {
        do {
            _ = try fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            if fm.fileExists(atPath: mainUrl!.path){
                do{
                    let jsonData = try Data(contentsOf: mainUrl!)
                    let actorsResponse = try JSONDecoder().decode(ActorListResponse.self, from: jsonData)
                    actors = actorsResponse.results
                    completion(actors)
                } catch {}
            } else {
                print(Error.self)
            }
        } catch {
            print(error)
        }
    }
}
