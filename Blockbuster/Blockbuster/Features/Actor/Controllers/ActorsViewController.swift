//
//  ActorsViewController.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 27/10/2022.
//

import UIKit
import Model

class ActorsViewController: UIViewController {
    
    @IBOutlet weak var actorTableView: UITableView!
    
    var actorViewModel : ActorViewModel?
    var selectedActor: Actor?
    var interactor: ActorBusinessLogic?
    
    @IBAction func segmentFavoriteControl(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            actorViewModel?.isFavoritesEnabled = false
            actorTableView.reloadData()
        } else {
            actorViewModel?.isFavoritesEnabled = true
            actorTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Actors"
        actorTableView.dataSource = self
        actorTableView.delegate = self
        actorTableView.reloadData()
        actorViewModel?.delegate = self
        fetchActors()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      
    }
    
    @objc func fetchActors() {
        actorViewModel?.fetchActors {
              DispatchQueue.main.async {
                  self.actorTableView.reloadData()
              }
          } failure: {
              print("error to get actors")
          }
      }

}

extension ActorsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedActor = actorViewModel?.actor(indexPath.row)
        //let vc = ActorsDetailViewController()
        let vc = ActorBioCleanArchitectureViewController()
        vc.selectedActor = selectedActor
       // vc.actorViewModel = self.actorViewModel
        self.present(vc, animated: true, completion: nil)        
    }
}

extension ActorsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActorCellIdentifier", for: indexPath) as? ActorTableViewCell
        else {
            return UITableViewCell()
        }
        let actor = actorViewModel?.actor(indexPath.row)
        cell.actor =  actor
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actorViewModel?.totalActors() ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension ActorsViewController:  ActorsViewModelDelegate {
    
    func fetchDidFail() {
        _ = UIAlertController(title: "Failed", message: "Try again", preferredStyle: .alert)
    }
    
    func didFetchActors() {
        self.actorTableView.reloadData()
    }
    
    func willFetchActor() {      
    }
}


