//
//  ActorsDetailViewController.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 31/10/2022.
//

import UIKit
import SnapKit
import Model

class ActorsDetailViewController: UIViewController {
    
    init(){
        super.init(nibName: nil, bundle: nil)
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var actorDetailViewModel = ActorDetailViewModel()
    var actorViewModel: ActorViewModel?
    var actor: Actor?
    
    var isFavorite: Bool {
        guard let actor = actor,
              let isFavorite = actorViewModel?.isFavoriteActor(actor)
        else {
            return false
        }
        return isFavorite
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
       
        self.title = "Actors Detail"
        
        guard let actor = actor else { return }
        
        let actorCard = ActorViewDetail.viewRepresentation(viewModel: self.actorDetailViewModel, viewController: self){ response in
            
            if response == "Back" {
                self.dismiss(animated: true)
            } else {
                self.upDateFavoriteData(favoriteState: self.isFavorite, actor: actor)
            }
        }
        self.view.addSubview(actorCard.view)
        
        actorCard.view.snp.makeConstraints{ make in
            make.height.width.equalToSuperview()
        }
    
        fetchActorsDetail( actor: actor)
    }
    
    func fetchActorsDetail(actor: Actor) {
        
        actorDetailViewModel.fetchActorDetail(withId: actor.id){
            
            if self.isFavorite{
                self.actorDetailViewModel.actorDetail?.isFavorite = "heart.fill"
            }
        }failure:{
            print("error to get actors Detail")
        }
    }
    
    func upDateFavoriteData(favoriteState: Bool, actor: Actor){
        
        guard let actorViewModel = self.actorViewModel  else { return }
        
        if favoriteState {
            actorViewModel.deleteActor(actor)
            actorDetailViewModel.actorDetail?.isFavorite = "heart"
        } else {
            actorViewModel.saveActorAsFavorite(actor)
            actorDetailViewModel.actorDetail?.isFavorite = "heart.fill"
        }
    }
}
