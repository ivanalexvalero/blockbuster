//
//  ActorCleanArchictectureController.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 23/11/2022.
//

import Foundation
import Model
import UIKit

protocol ActorDisplayLogic: class {
    func displayActorList(viewModel: Actors.ActorList.ViewModel)
}

class ActorCleanArchictectureController: UIViewController, ActorDisplayLogic{
    
    @IBOutlet weak var actorTableView: UITableView!
    
    let actorViewModel = ActorCleanViewModel()
    var selectedActor: Actor?
    var interactor: ActorBusinessLogic?
    var router: (NSObjectProtocol & ActorRoutingLogic & ActorDataPassing)?
    
    @IBAction func segmentFavoriteControl(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
           // actorViewModel.isFavoritesEnabled = false
            requestActor()
            actorTableView.reloadData()
        } else {
            //actorViewModel.isFavoritesEnabled = true
            getActorCore()
            actorTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Actors"
        actorTableView.dataSource = self
        actorTableView.delegate = self
        actorTableView.reloadData()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        requestActor()
    }
    
    private func setup() {
        let viewController = self
        let interactor =  ActorInteractor()
        let presenter = ActorPresenter()
        let router =    ActorRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
       
    func displayActorList(viewModel: Actors.ActorList.ViewModel) {
        if viewModel.errorMessage.isEmpty{
            self.actorViewModel.updateActorList(viewModel.results)
            actorTableView.reloadData()
        }else{
            alertMessage(title: "Error", message: viewModel.errorMessage)
        }
    }
    
    func requestActor() {
        let request = Actors.ActorList.Request()
      interactor?.fetchActorNetwork(request: request)
    }
    
    func getActorCore() {
        let request = Actors.ActorList.Request()
        interactor?.getActorCore(request: request)
    }
    
    func alertMessage(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
        NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension ActorCleanArchictectureController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedActor = actorViewModel.actor(indexPath.row)
        let vc = ActorBioCleanArchitectureViewController()
        vc.selectedActor = selectedActor
        self.present(vc, animated: true, completion: nil)
    }
}

extension ActorCleanArchictectureController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActorCellIdentifier", for: indexPath) as? ActorTableViewCell
        else {
            return UITableViewCell()
        }
        let actor = actorViewModel.actor(indexPath.row)
        cell.actor =  actor
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actorViewModel.totalActors()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}


