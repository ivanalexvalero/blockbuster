//
//  ActorCleanViewModel.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 23/11/2022.
//

import Foundation
import Model

class ActorCleanViewModel{
    
    var actors : [Actor] = []
    var delegate: ActorsViewModelDelegate?
    var isFavoritesEnabled: Bool = false
   
    func actorsDataSource() -> [Actor]{
        return actors
    }
    
    func actor(_ index: Int) -> Actor? {
        return actors[index]
    }
    
    func totalActors() -> Int {
        return actors.count
    }
    
    func saveActorAsFavorite(_ actor: Actor) {
        DatabaseManager.shared.save(entity: .actor, parameters: actor.dictionaryRepresentation)
    }
    
    func fetchFavorites() -> [Actor] {
        let actors = DatabaseManager.shared.fetch(entity: .actor, queryType: .all).map({ object -> Actor in
            Actor(name: object.value(forKey: "name") as! String,
                  popularity: object.value(forKey: "popularity") as! Double,
                  id: object.value(forKey: "id") as! Int,
                  image: object.value(forKey: "imageURL") as? String)
        })
        return actors
    }
    
    func deleteActor(_ actor: Actor) {
        DatabaseManager.shared.delete2(entity: .actor, value: actor.name)
    }

    func isFavoriteActor(_ actor: Actor) -> Bool {
        return fetchFavorites().contains { $0.id == actor.id }
    }
    
    func updateActorList(_ newMedia: [Actor]) {
        self.actors = newMedia
        delegate?.didFetchActors()
    }
}
