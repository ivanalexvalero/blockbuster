//
//  ActorBioCleanArchitectureModels.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 18/11/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import Model

enum ActorBioCleanArchitectureModel
{
  // MARK: Use cases
  
  enum ActorBioClean
  {
    struct Request
    {
    }
    struct Response
    {
        var response: ActorBio
    }
    struct ViewModel
    {
        var result: ActorBio
    }
  }
}
