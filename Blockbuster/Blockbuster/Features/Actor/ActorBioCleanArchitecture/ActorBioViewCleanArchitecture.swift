//
//  ActorBioViewCleanArchitecture.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 18/11/2022.
//
import SwiftUI
import Kingfisher
import Model

public struct ActorBioViewCleanArchitecture: View{
    
    @ObservedObject var actorBioObservable: ActorCleanArchitectureObservable

    var action: (String) -> ()
           
    enum Actions: String{
        case back = "Back"
        case favorite = "Favorite"
    }
    
    public var body: some View {
        VStack {
            HStack {
                Button { action(Actions.back.rawValue) }
            label: {
                    Circle()
                        .frame(width: 40, height: 40)
                        .clipped()
                        .foregroundColor(Color(.systemBackground))
                        .overlay {
                            Image(systemName: "arrow.backward")
                            .foregroundColor(.primary)
                            .font(.title3)
                        }
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .clipped()
                        .padding()
                }
                Button{
                    action(String(actorBioObservable.actorBio?.id ?? 0))
                    print("like")
                }label:{
                    Circle()
                        .frame(width: 40, height: 40)
                        .clipped()
                        .foregroundColor(Color(.systemBackground))
                        .overlay {
                            Image(systemName: actorBioObservable.actorBio?.isFavorite ?? "heart")
                            .foregroundColor(.primary)
                            .font(.title3)
                        }
                        .frame(maxWidth: .infinity, alignment:.trailing)
                        .clipped()
                        .padding()
                }
               
            }
            .padding(.top, 44)
            .frame(maxWidth: .infinity, maxHeight: 300, alignment: .top)
            .clipped()
            .background {
                KFImage(actorBioObservable.actorBio?.imageURLRepresentation())
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fill)
            }
            .mask {
                Rectangle()
            }
            VStack(alignment: .leading, spacing: 4) {
                HStack(alignment: .firstTextBaseline) {
                    Text(actorBioObservable.actorBio?.name ?? "-")
                        .font(.system(size: 29, weight: .semibold, design: .default))
                    Spacer()
                    HStack(alignment: .firstTextBaseline, spacing: 3) {
                        Image(systemName: "star")
                            .foregroundColor(.orange)
                        Text(String(actorBioObservable.actorBio?.popularity ?? 0.0))
                    }
                    .font(.system(size: 16, weight: .regular, design: .monospaced))
                }
                Text(actorBioObservable.actorBio?.birthday ?? "-")
                    .font(.system(size: 15, weight: .light, design: .default))
                Text(actorBioObservable.actorBio?.biography ?? "-")
                    .font(.system(size: 14, weight: .regular, design: .serif))
                    .padding(.top, 4)
            }
            .padding(.horizontal, 24)
            .padding(.vertical, 12)
            VStack(alignment: .leading, spacing: 15) {
                HStack(spacing: 9) {
                    Spacer()
                }
                .font(.subheadline)
                HStack(spacing: 9) {
                    Spacer()
                }
                .font(.subheadline)
                HStack(spacing: 9) {
                    Spacer()
                }
                .font(.subheadline)
                HStack(spacing: 9) {
                    Spacer()
                }
                .font(.subheadline)
            }
            .padding(.horizontal, 24)
            HStack {
                Spacer()
            }
            .padding(24)
            .foregroundColor(.orange)
            Spacer()
            VStack(spacing: 14) {
                HStack(spacing: 4) {
                    
                }
            }
        }
    }
}

extension ActorBioViewCleanArchitecture{
    
     static func viewRepresentation(actorBioObservable: ActorCleanArchitectureObservable, viewController: UIViewController,  action: @escaping (String) -> ()) -> UIHostingController<ActorBioViewCleanArchitecture>{
        
         let childView = UIHostingController(rootView: ActorBioViewCleanArchitecture(actorBioObservable: actorBioObservable,  action: action))
        
        viewController.view.addSubview(childView.view)
        childView.didMove(toParent: viewController)
        return childView
    }
}
