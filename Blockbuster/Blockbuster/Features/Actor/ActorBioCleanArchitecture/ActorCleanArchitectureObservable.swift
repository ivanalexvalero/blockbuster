//
//  ActorCleanArchitectureViewModel.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 18/11/2022.
//

import Foundation
import Model

class ActorCleanArchitectureObservable: ObservableObject {
    @Published var actorBio : ActorBio?
}
