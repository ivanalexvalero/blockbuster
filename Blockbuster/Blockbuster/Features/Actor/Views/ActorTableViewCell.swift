//
//  ActorTableViewCell.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 27/10/2022.
//

import UIKit
import Kingfisher
import Model

class ActorTableViewCell: UITableViewCell {

    @IBOutlet weak var actorImageView: UIImageView!
    @IBOutlet weak var nameActorLabel: UILabel!
    @IBOutlet weak var popularityActorLabel: UILabel!
    
    var actor : Actor?{
        didSet{
            guard let actor = actor else{
                return
            }
            nameActorLabel.text = actor.name
            popularityActorLabel.text = "Popularity: " + String(actor.popularity)
            actorImageView.kf.setImage(with: actor.imageURLRepresentation())
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

