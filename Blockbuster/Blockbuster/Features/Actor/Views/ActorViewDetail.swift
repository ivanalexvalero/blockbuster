//
//  ActorView.swift
//  Blockbuster
//
//  Created by Cristian Gonzalez on 31/10/2022.
//

import SwiftUI
import Kingfisher
import Model

public struct ActorViewDetail: View {
    
    @ObservedObject var viewModel: ActorDetailViewModel

    var action: (String) -> ()
           
    enum Actions: String{
        case back = "Back"
        case favorite = "Favorite"
    }
    
    public var body: some View {
        VStack {
            HStack {
                Button { action(Actions.back.rawValue) }
            label: {
                    Circle()
                        .frame(width: 40, height: 40)
                        .clipped()
                        .foregroundColor(Color(.systemBackground))
                        .overlay {
                            Image(systemName: "arrow.backward")
                            .foregroundColor(.primary)
                            .font(.title3)
                        }
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .clipped()
                        .padding()
                }
                Button{
                    action(String(viewModel.actorDetail?.id ?? 0))
                    print("like")
                }label:{
                    Circle()
                        .frame(width: 40, height: 40)
                        .clipped()
                        .foregroundColor(Color(.systemBackground))
                        .overlay {
                            Image(systemName: viewModel.actorDetail?.isFavorite ?? "heart")
                            .foregroundColor(.primary)
                            .font(.title3)
                        }
                        .frame(maxWidth: .infinity, alignment:.trailing)
                        .clipped()
                        .padding()
                }
               
            }
            .padding(.top, 44)
            .frame(maxWidth: .infinity, maxHeight: 300, alignment: .top)
            .clipped()
            .background {
                KFImage(viewModel.actoDetail()?.imageURLRepresentation())
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fill)
            }
            .mask {
                Rectangle()
            }
            VStack(alignment: .leading, spacing: 4) {
                HStack(alignment: .firstTextBaseline) {
                    Text(viewModel.actorDetail?.name ?? "-")
                        .font(.system(size: 29, weight: .semibold, design: .default))
                    Spacer()
                    HStack(alignment: .firstTextBaseline, spacing: 3) {
                        Image(systemName: "star")
                            .foregroundColor(.orange)
                        Text(String(viewModel.actorDetail?.popularity ?? 0.0))
                    }
                    .font(.system(size: 16, weight: .regular, design: .monospaced))
                }
                Text(viewModel.actorDetail?.birthday ?? "-")
                    .font(.system(size: 15, weight: .light, design: .default))
                Text(viewModel.actorDetail?.biography ?? "-")
                    .font(.system(size: 14, weight: .regular, design: .serif))
                    .padding(.top, 4)
            }
            .padding(.horizontal, 24)
            .padding(.vertical, 12)
            VStack(alignment: .leading, spacing: 15) {
                HStack(spacing: 9) {
                    Spacer()
                }
                .font(.subheadline)
                HStack(spacing: 9) {
                    Spacer()
                }
                .font(.subheadline)
                HStack(spacing: 9) {
                    Spacer()
                }
                .font(.subheadline)
                HStack(spacing: 9) {
                    Spacer()
                }
                .font(.subheadline)
            }
            .padding(.horizontal, 24)
            HStack {
                Spacer()
            }
            .padding(24)
            .foregroundColor(.orange)
            Spacer()
            VStack(spacing: 14) {
                HStack(spacing: 4) {
                    
                }
            }
        }
    }
}

extension ActorViewDetail{
    
     static func viewRepresentation(viewModel: ActorDetailViewModel, viewController: UIViewController,  action: @escaping (String) -> ()) -> UIHostingController<ActorViewDetail>{
        
        let childView = UIHostingController(rootView: ActorViewDetail(viewModel: viewModel,  action: action))
        
        viewController.view.addSubview(childView.view)
        childView.didMove(toParent: viewController)
        return childView
    }
}
 
