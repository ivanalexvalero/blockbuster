//
//  ReviewsWorker.swift
//  Blockbuster
//
//  Created by Juleanny Navas on 16/11/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import Moya
import APIService
import Model

class ReviewsWorker {
  
    func fetchReviewsMovie(withId id: Int, success: @escaping (ReviewResponse) -> (), failure: @escaping () -> ()) {
        let provider = MoyaProvider<TheMovieDBAPI>()
        provider.request(.moviesReviews(id)) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let response = try JSONDecoder().decode(ReviewResponse.self, from: moyaResponse.data)
                    success(response)
                    print(response.results)
                } catch {
                    print(error.localizedDescription)
                    failure()
                }
            case let .failure(error):
                print(error.localizedDescription)
                failure()
            }
        }
    }
}
