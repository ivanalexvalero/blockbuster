//
//  ReviewsViewModel.swift
//  Blockbuster
//
//  Created by Juleanny Navas on 26/10/2022.
//

import Foundation
import Moya
import SystemDesign
import APIService
import Model

class ReviewsViewModel {
    var reviews: ReviewResponse?
    
    func fetchReviewsMovie(withId id: Int, success: @escaping () -> (), failure: @escaping () -> ()) {
        let provider = MoyaProvider<TheMovieDBAPI>()
        provider.request(.moviesReviews(id)) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let reviews = try JSONDecoder().decode(ReviewResponse.self, from: moyaResponse.data)
                    self.reviews = reviews
                    print(reviews.results)
                } catch {
                    print(error.localizedDescription)
                    failure()
                }
            case let .failure(error):
                print(error.localizedDescription)
                failure()
            }
        }
    }
}
