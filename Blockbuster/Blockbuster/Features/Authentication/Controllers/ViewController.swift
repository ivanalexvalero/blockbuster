//
//  ViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 19/10/22.
//

import UIKit
import SystemDesign
import SnapKit

class ViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    let viewModel = LoginViewModel()
    let spinner = LoadingIndicator.spinner

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(spinner)
        spinner.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(emailTextField.snp.top).offset(-20)
            make.height.width.equalTo(50)
        }
        spinner.hidesWhenStopped = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if viewModel.userIsActive() {
            self.performSegue(withIdentifier: "MoviesSegue", sender: self)

        }
    }
    
    @IBAction func presentDetails(_ sender: Any) {
        guard let email = emailTextField.text,
              let password = passwordTextField.text else {
            return
        }
        spinner.startAnimating()
        viewModel.performSignIn(email: email, password: password) { response in
            self.spinner.stopAnimating()
            if response == true {
                self.performSegue(withIdentifier: "MoviesSegue", sender: self)
            } else {
                //present alert with error
            }
        }
        
    }
    
    @IBAction func performSignIn(_ sender: Any) {
       /*
        if (emailTextField.text == defaultUser.email) && (passwordTextField.text == defaultUser.password) {
            print("Welcome back", defaultUser.name)
        } else {
            print("Login Failed, try again")
        }
        */
    }
    
}

