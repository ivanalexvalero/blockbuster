//
//  LoginViewModel.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 26/10/22.
//

import Foundation
import APIService
import SystemDesign
import Moya

class LoginViewModel {
    var email: String = ""
    var password: String = ""
    
    func areEmailAndPasswordValid(email: String, password: String) -> Bool {
        return email.count > 0 && password.count > 0
    }
    
    func userIsActive() -> Bool {
        return UserDefaults.standard.string(forKey: "email") != nil ? true : false
    }
    
    func performSignIn(email: String, password: String, completion: @escaping (Bool) -> Void) {
        guard areEmailAndPasswordValid(email: email, password: password) else {
            completion(false)
            return
        }
        
        self.email = email
        self.password = password
        
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.set(password, forKey: "password")
        
        let provider = MoyaProvider<AuthenticatorAPI>()
        provider.request(.login(email: self.email, password: self.password)) { result in
            print(result)
            // TODO: - Store user credentials in Keychain
           completion(true)
        }
    }
}
