//
//  SettingsViewController.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 26/10/22.
//

import UIKit
import SnapKit
import SystemDesign

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        let signOutButton = UIButton(frame: .zero)
        view.addSubview(signOutButton)
        signOutButton.setTitle("Profile", for: .normal)
        signOutButton.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(40)
            make.centerY.equalToSuperview()
        }
        signOutButton.addTarget(self, action: #selector(showProfileSettings), for: .touchUpInside)
        signOutButton.adoptStyle(style: .orange)
        
        let logoImageView = UIImageView.appLogoImageView()
        view.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { make in
            make.top.equalTo(100)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(100)
        }
    }

    @IBAction @objc func showProfileSettings(_ sender: Any) {
        let vc = ProfileViewController()
        self.present(vc, animated: true)
        
        
    }
    
}
