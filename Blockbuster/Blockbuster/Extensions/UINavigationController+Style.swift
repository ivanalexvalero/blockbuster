//
//  UINavigationController+Style.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 24/11/22.
//

import UIKit

extension UINavigationController {
    static func generate(controller: UIViewController, image: UIImage, title: String) -> UINavigationController {
        let nav = UINavigationController(rootViewController: controller)
        nav.tabBarItem = UITabBarItem(title: title, image: image, selectedImage: image)
        nav.tabBarItem.title = title
        nav.title = title
        return nav
    }
}
