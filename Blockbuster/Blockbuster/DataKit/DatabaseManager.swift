//
//  DatabaseManager.swift
//  Blockbuster
//
//  Created by Bryan A Bolivar M on 1/11/22.
//

import Foundation
import CoreData
import UIKit
import Model

enum EntityType: String {
    case movie = "MovieEntity"
    case tvShow = "TvShowEntity"
    case actor = "ActorEntity"
    
    var primaryKey: String {
        switch self {
        case .movie, .tvShow:
            return "name"
        case .actor:
            return "id"
        }
    }
}

enum QueryType {
    case all
    case filter(attribute: String, value: String)
}


class DatabaseManager {
    
    ///Singleton
    static let shared: DatabaseManager = DatabaseManager()
    
    private init() { }
    
    func save(entity: EntityType, parameters: [String: Any]) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: entity.rawValue, in: managedContext)!
        let object = NSManagedObject(entity: entity, insertInto: managedContext)
        
        for key in parameters.keys {
            object.setValue(parameters[key], forKey: key)
        }
        
        do {
            try managedContext.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func fetch(entity: EntityType, queryType: QueryType) -> [NSManagedObject] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return []
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entity.rawValue)
        
        //TODO: - Enable QueryType using NSPredicate
        
        do {
            let response = try managedContext.fetch(fetchRequest)
            return response
        } catch {
            
        }
        return []
    }
    
    //This method delete all favorites
    func delete(entity: EntityType, value: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let results = fetch(entity: entity, queryType: .filter(attribute: entity.primaryKey, value: value))
        let managedContext = appDelegate.persistentContainer.viewContext
        do {
            for object in results {
                managedContext.delete(object)
            }
            try managedContext.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    //This method delete only one favorite
    func delete2(entity: EntityType, value: String){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
        fetchRequest.predicate = NSPredicate(format: "name = %@", value)
        
        do {
            let test = try managedContext.fetch(fetchRequest)
            let objectToDelete = test[0] as! NSManagedObject
            managedContext.delete(objectToDelete)
            try managedContext.save()
            
        } catch {
            print(error.localizedDescription)
        }
    }
}
